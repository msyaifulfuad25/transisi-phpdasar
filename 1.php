<?php 
    $data_nilai = "72 65 73 78 75 74 90 81 87 65 55 69 72 78 79 91 100 40 67 77 86";
    $nilai = explode(' ', $data_nilai);
    
    echo 'Rata rata : '.rataRata($nilai).'<br>';
    echo 'Nilai tertinggi : '.cariNilai($nilai, 1).'<br>';
    echo 'Nilai terendah : '.cariNilai($nilai, 0).'<br>';

    function rataRata($nilai) {
        return $result = array_sum($nilai)/count($nilai);
    }

    function cariNilai($nilai, $cek) {
        if ($cek == 0)
            $hasil_nilai = max($nilai);
        else
            $hasil_nilai = min($nilai);
        return $hasil_nilai;
    }

    ###################################

    $huruf = 'TranSISI TekNoLogi ManDIrI';
    echo "Jumlah huruf kecil dari ($huruf) : ".countLower('TranSISI').'<br>';

    function countLower($word) {
      return strlen(preg_replace('![^a-z]+!', '', $word));
    }

    ###################################

    $data_kalimat = 'Jakarta adalah ibukota negara Republik Indonesia';
    $kalimat = explode(' ', $data_kalimat);

    echo cariUBT($kalimat);

    function cariUBT($kalimat) {
        $unigram = '';
        $bigram = '';
        $trigram = '';
        foreach ($kalimat as $k => $key) {
            if (($k+1) % 1 == 0) {
                $unigram .= $key.', ';
                
                if (($k+1) % 2 == 0) {
                    $bigram .= $key.', ';
                } else {
                    $bigram .= $key.' ';
                }

                if (($k+1) % 3 == 0) {
                    $trigram .= $key.', ';
                } else {
                    $trigram .= $key.' ';
                }
            }
        }

        return 'Unigram : '.$unigram.'<br>'.'Bigram : '.$bigram.'<br>'.'Trigram : '.$trigram;
    }

?>