<?php 
    showTable();

    function showTable() {
        $color = FALSE;
        $back_color = FALSE;
        $text_color = FALSE;
        $schema = array(2,2,1,1,1,2,2,1);
        $position = 0;
        $number = 1;

        $table = '<table border="1" style="border-collapse: collapse;"><tr>';

        for ($i=1; $i <= 64; $i++) {
            if ($color == FALSE) {
                $back_color = 'white';
                $text_color = 'black';
            } else {
                $back_color = 'black';
                $text_color = 'white';
            }

            $check = $schema[$position];
            $table .= '<td style="background-color: '.$text_color.'; color: '.$back_color.'; padding: 10px">'.$i.'</td>';
            if ($number == $check) {
                if ($color == FALSE) {
                    $color = TRUE;
                } else {
                    $color = FALSE;
                }
                if ($position > 6) {
                    $position = 0;
                } else {
                    $position = $position+1;
                }
                $number = 0;
            }

            if ($i % 8 == 0) {
                $table .= "</tr><tr>";
            }
            $number++;
        }

        $table .= '</table>';

        echo $table;
    }
?>