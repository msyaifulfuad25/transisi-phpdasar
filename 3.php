<?php  
    $arr = array(
        ['f', 'g', 'h', 'i'],
        ['j', 'k', 'p', 'q'],
        ['r', 's', 't', 'u'],
    );

    cari($arr, 'fghi');
    cari($arr, 'fghp');
    cari($arr, 'fjrstp');

    function cari($array, $word) {
        $fix_arr = fixArr($array);
        $split_word = str_split($word);
        $result = TRUE;
        
        foreach ($split_word as $a) {
            if (!in_array($a, $fix_arr)) {
                echo "$word = FALSE<br>";
                exit;
            }
        }
        echo "$word = TRUE<br>";
    }

    function fixArr($array) {
        $result = array();
        for ($i=0; $i < count($array); $i++) {
            foreach ($array[$i] as $a) {
                array_push($result, $a);
            } 
        }
        return $result;
    }
?>